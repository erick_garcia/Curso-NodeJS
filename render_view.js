function renderView( data, aParametros ){ 
    var html = data.toString(); 
    var variables = html.match(/[^\{\}]+(?=\})/g); 
    for( var i = 0; i < variables.length; i++ ){ 
        html = html.replace( "{" + variables[ i ] + "}", aParametros[ variables[ i ] ] ); 
    } 
    return html; 
}
module.exports.renderView = renderView;