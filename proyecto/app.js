var express = require('express');
var bodyParser = require('body-parser');
var User = require("./models/user").User;
var session = require('express-session');
var router_app = require('./routes_app');
var session_middleware = require('./middlewares/session'); 
var app = express();

app.use('/public', express.static('public'));
app.use(bodyParser.json()); //para peticiones application/json
app.use(bodyParser.urlencoded({extended: true}));

app.use(session({
    secret: "12243ffdfdfsdfsd",
    resave: false,
    saveUninitialized: false
}));

//app.use(express.static('assets'));
app.set("view engine", "jade");

app.get("/", function(req, res) {
    console.log(req.session.user_id);
    res.render('index');
});

app.get("/signup", function(req, res) {
    User.find(function(err, doc){
        console.log(doc);
        res.render('signup');
    });
});

app.get("/login", function(req, res) {
    res.render('login');
});

app.post("/users", function(req, res) {
    
    //console.log("Contraseña: " + req.body.password);
    //console.log("Email: " + req.body.email);
    var user = new User({email: req.body.email, 
                        password: req.body.password, 
                        password_confirmation: req.body.password_confirmation,
                        username: req.body.username
                    });
    /*console.log(user.password_confirmation);
    user.save(function(err){
        if(err){
            console.log(String(err));
        }
        res.send('Guardamos tus datos');
    });*/
    user.save().then(function(us){
        res.send("Guardamos exitosamente los datos");
    }, function(err) {
        if(err) {
            console.log(String(err));
            res.send("No pudimos guardar la información");
        }
    });
});

app.post("/sessions", function(req, res) {
    User.findOne({email: req.body.email, password: req.body.password}, function(err, user){
        //console.log(docs);
        req.session.user_id = user._id;
        res.redirect('/app');
    });
});
app.use("/app", session_middleware);
app.use("/app", router_app);
app.listen(8080);